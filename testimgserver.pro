TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp


unix:!macx: LIBS += -L/usr/lib/x86_64-linux-gnu/ -lzmq -lopencv_highgui -lopencv_imgproc -lopencv_core -lopencv_imgcodecs -lrt -lpthread

INCLUDEPATH += $$PWD/../../../../../usr/local/ButlerIPU/include
DEPENDPATH += $$PWD/../../../../../usr/local/ButlerIPU/include
