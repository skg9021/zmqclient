#include <iostream>
#include <zmq.hpp>

#include <chrono>
#include <thread>

#include <boost/interprocess/ipc/message_queue.hpp>

#include <opencv2/highgui.hpp>

using namespace boost::interprocess;

int main() {
  cv::Mat img0 =
      cv::imread("/home/saket/images/BotCam400_0.bmp", CV_LOAD_IMAGE_GRAYSCALE);
  cv::Mat img1 =
      cv::imread("/home/saket/images/BotCam375_0.bmp", CV_LOAD_IMAGE_GRAYSCALE);
  if (img0.empty() || img1.empty())
    return -1;

  cv::Mat img[2] = {img1, img0};
  zmq::context_t context(1);
  //  Socket to receive messages on
  zmq::socket_t receiver(context, ZMQ_PULL);
  receiver.connect("tcp://localhost:5551");
  // receiver.setsockopt(ZMQ_RCVTIMEO, 15);

  //  Socket to send messages to
  zmq::socket_t sender(context, ZMQ_PUSH);
  sender.bind("tcp://*:5552");

  //  The first message is "0" and signals start of batch
  //  zmq::socket_t sink(context, ZMQ_PUSH);
  //  sink.connect("tcp://localhost:5558");
  //  zmq::message_t message(2);
  //  memcpy(message.data(), "0", 1);
  //  sink.send(message);

  std::cout << "Connecting to hello world server..." << std::endl;
  // socket.connect("tcp://localhost:5556");
  zmq::pollitem_t items[] = {{receiver, 0, ZMQ_POLLIN, 0}};
  int cnt = 0;
  while (true) {
    std::cout << "Creating messages" << std::endl;

    //    zmq_msg_t msg;
    //    zmq_msg_init_size(&msg, img.rows * img.cols);
    //    memcpy(zmq_msg_data(&msg), img.data, img.rows * img.cols);

    //    zmq::message_t request(img.cols * img.rows);
    //    zmq_msg_init(msg);
    //    zmq_msg_init_size(msg, img.cols * img.rows);

    //    memcpy(request.data(), img.data, img.cols * img.rows);

    // zmq_send(socket.)
    sender.send((void *)img[cnt].data, img[cnt].cols * img[cnt].rows);

    zmq::message_t reply;
    if (receiver.connected()) {
      std::cout << "Connected" << std::endl;
    }
    int rc = zmq::poll(&items[0], 1, 15);
    if (items[0].revents & ZMQ_POLLIN) {
      receiver.recv(&reply);
      if (reply.size() > 0) {
        std::string rpl =
            std::string(static_cast<char *>(reply.data()), reply.size());
        std::cout << "Received fuck off: " << rpl << std::endl;
      }
    }

    cnt++;
    cnt = cnt %2;
    std::this_thread::sleep_for(std::chrono::milliseconds(20));
  }

  return 0;
}
